package com.app.esperancia.icones;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.liulishuo.magicprogresswidget.MagicProgressCircle;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cn.dreamtobe.percentsmoothhandler.ISmoothTarget;

/**
 * Created by esperancia on 02/06/2017.
 */

public class PetitionsFragment extends Fragment {

    private ArrayList<Petition> rentalProperties = new ArrayList<>();

    public static PetitionsFragment newInstance() {
        PetitionsFragment fragment = new PetitionsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //create property elements
        rentalProperties.add(
                new Petition("profil", "Espah Pouch", "Il y a 3 jours", "Benin", "fille", "Aminata, fille de 8 ans voudrait bien devenir styliste.\n" +
                        "Pour le moment, elle a interrompu ses études et vend des cure-dents au marché Dantokpa", 81 ));

        rentalProperties.add(
                new Petition("avatar1", "EspoirSansFrontière/Pierre Lefèvre", "Il y a 2 jours", "France", "filles", "Au Vietnam, bon nombre de filles ont besoin de soutien pour avoir une vie meilleure.\n" +
                        "Redonnez leur la joie de vivre.", 106 ));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_layout, container, false);

        ArrayAdapter<Petition> adapter = new PetitionsArrayAdapter(getActivity(), 0, rentalProperties);

        ListView mList = (ListView) v.findViewById(R.id.customListView);
        mList.setAdapter(adapter);

        return v;
    }
}


//Base class to hold information about the petition
class Petition {

    //Petition basics

    private String Avatar;
    private String Nom;
    private String DatePub;
    private String Pays;
    private String ImagePetition;
    private String Histoire;
    private Integer ProgressStatus;

    //constructor
    public Petition(String Avatar, String Nom, String DatePub, String Pays, String ImagePetition, String Histoire, Integer ProgressStatus) {

        this.Avatar = Avatar;
        this.Nom = Nom;
        this.DatePub = DatePub;
        this.Pays = Pays;
        this.ImagePetition = ImagePetition;
        this.Histoire = Histoire;
        this.ProgressStatus = ProgressStatus;
    }

    //getters
    public String getAvatar() {
        return Avatar;
    }

    public String getNom() {
        return Nom;
    }

    public String getDatePub() {
        return DatePub;
    }

    public String getPays() {
        return Pays;
    }

    public String getImagePetition() {
        return ImagePetition;
    }

    public String getHistoire() {
        return Histoire;
    }

    public Integer getProgressStatus() {
        return ProgressStatus;
    }

}

//
class PetitionsArrayAdapter extends ArrayAdapter<Petition> {

    private Context context;
    private List<Petition> rentalProperties;

    private Handler handler = new Handler();

    //constructor, call on creation
    public PetitionsArrayAdapter(Context context, int resource, ArrayList<Petition> objects) {

        super(context, resource, objects);

        this.context = context;
        this.rentalProperties = objects;
    }

    //called when rendering the list
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public View getView(int position, View convertView, ViewGroup parent) {

        //get the petition we are displaying
        final Petition petition = rentalProperties.get(position);

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.petition_lay, null);

        ImageView avatar = (ImageView) view.findViewById(R.id.avatar);
        TextView nom = (TextView) view.findViewById(R.id.nom);
        TextView datePub = (TextView) view.findViewById(R.id.temps);
        TextView pays = (TextView) view.findViewById(R.id.pays);
        TextView histoire = (TextView) view.findViewById(R.id.resume);
        //View blocPetition = (View) view.findViewById(R.id.petitionbloc);
        ImageView petitionImage = (ImageView) view.findViewById(R.id.petitionImg);
        final MagicProgressCircle mProgressBar = (MagicProgressCircle) view.findViewById(R.id.circle_progress_bar);
        final AnimTextView progressValue = (AnimTextView) view.findViewById(R.id.demo_tv);

        //anim(mProgressBar, progressValue);
        //display trimmed excerpt for description(story)
        int histoireLength = petition.getHistoire().length();
        if(histoireLength >= 200){
            String histoireTrim = petition.getHistoire().substring(0, 200) + "...";
            histoire.setText(histoireTrim);
        }else{
            histoire.setText(petition.getHistoire());
        }

        //set price and rental attributes
        nom.setText(petition.getNom());
        datePub.setText(petition.getDatePub());
        pays.setText(petition.getPays());
        histoire.setText(petition.getHistoire());

        //get the image associated with this petition
        int avatarID = context.getResources().getIdentifier(petition.getAvatar(), "drawable", context.getPackageName());
        avatar.setImageResource(avatarID);

        int imageBgId = context.getResources().getIdentifier(petition.getImagePetition(), "drawable", context.getPackageName());

        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            // blocPetition.setBackground(ResourcesCompat.getDrawable(context.getResources(), imageBgId, null));
        // } else {
            // blocPetition.setBackground(ContextCompat.getDrawable(context, imageBgId));
        // }

        petitionImage.setImageResource(imageBgId);

        float mpcPercent = getIncreasedPercent(mProgressBar);
        mProgressBar.setPercent((float) ((position+1) * 0.2));
        progressValue.setText( (position*20 + 20)+"%");

        //mProgressBar.setPercent((float) (petition.getProgressStatus() * 0.2));
        //progressValue.setText( (petition.getProgressStatus()*20)+"\n %");


        return view;
    }

    private boolean isAnimActive;
    private final Random random = new Random();

    private void anim(MagicProgressCircle mProgressBar, AnimTextView progressView ) {
        final int ceil = 26;
        final int progress = random.nextInt(ceil);

        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(mProgressBar, "percent", 0, progress / 100f),
                ObjectAnimator.ofInt(progressView, "progress", 0, progress)
        );
        set.setDuration(600);
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                isAnimActive = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isAnimActive = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        set.setInterpolator(new AccelerateInterpolator());
        set.start();
    }

    private float getIncreasedPercent(ISmoothTarget target) {
        float increasedPercent = target.getPercent() + 0.1f;

        return Math.min(1, increasedPercent);
    }


}
