package com.app.esperancia.icones;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {

    private View mContentView;

    private View mControlsView;

    private boolean mVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);

        TextView aproposLink = (TextView) findViewById(R.id.apropos);
        aproposLink.setMovementMethod(LinkMovementMethod.getInstance());
        Linkify.addLinks(aproposLink, Linkify.WEB_URLS);


        TextView btnCommencer = (TextView) findViewById(R.id.btnCommencer);
        btnCommencer.setAutoLinkMask(Linkify.ALL);
        btnCommencer.setLinksClickable(true);
        btnCommencer.setMovementMethod(LinkMovementMethod.getInstance());

        btnCommencer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(FullscreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
    }

}
