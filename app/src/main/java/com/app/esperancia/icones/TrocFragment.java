package com.app.esperancia.icones;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by esperancia on 02/06/2017.
 */

public class TrocFragment extends Fragment {

    private ArrayList<Troc> rentalProperties = new ArrayList<>();

    public static TrocFragment newInstance() {
        TrocFragment fragment = new TrocFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //create property elements
        rentalProperties.add(
                new Troc("Les bouts de bois de Dieu", "Littérature", "Espah Pouch", "Manager Rightcom", "A OFFRIR", "profil", "Ousmane Sembène", "Edition Premium 1996"));

        rentalProperties.add(
                new Troc("Physique chimie Terminal S", "Physique/Chimie", "Deny Diego", "Etudiant en Sciences Juridiques", "A LOUER", "avatar2", "Adolphe TOMASINO", "Edition NATHAN / 2002"));

        rentalProperties.add(
                new Troc("Les maths pour les nuls Première S", "Mathématiques", "Jacob Hounguè", "Elève en première S", "A ECHANGER", "avatar3", "Jean-Louis BOURSIN", "Edition 2005"));

        rentalProperties.add(
                new Troc("Candide", "Littérature", "Lucie d'Olivera", "Etudiant en lettres moderne", "A OFFRIR", "avatar", "Voltaire", "Le Livre de Poche, 1995"));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_layout, container, false);

        ArrayAdapter<Troc> adapter = new TrocArrayAdapter(getActivity(), 0, rentalProperties);

        ListView mList = (ListView) v.findViewById(R.id.customListView);
        mList.setAdapter(adapter);

        return v;
    }
}


//Base class to hold information about the petition
class Troc {

    //Petition basics
    private String Titre;
    private String Categorie;
    private String Details;

    private String Nom;
    private String Statut;
    private String Action;
    private String Auteur;
    private String Edition;
    private String Avatar;

    //constructor
    public Troc(String Titre, String Categorie, String Nom, String Statut, String Action, String Avatar, String Auteur, String Edition){

        this.Categorie = Categorie;
        this.Titre = Titre;
        this.Nom = Nom;
        this.Statut = Statut;
        this.Action = Action;
        this.Auteur = Auteur;
        this.Edition = Edition;
        this.Avatar = Avatar;
    }

    //getters
    public String getTitre() { return Titre; }
    public String getCategorie() { return Categorie; }
    public String getDetails() { return Details; }
    public String getNom() { return Nom; }
    public String getStatut() { return Statut; }
    public String getAction() { return Action; }
    public String getAuteur() { return Auteur; }
    public String getEdition() { return Edition; }
    public String getImage() { return Avatar; }

}


//
class TrocArrayAdapter extends ArrayAdapter<Troc> {

    private Context context;
    private List<Troc> rentalProperties;

    //constructor, call on creation
    public TrocArrayAdapter(Context context, int resource, ArrayList<Troc> objects) {

        super(context, resource, objects);

        this.context = context;
        this.rentalProperties = objects;
    }

    //called when rendering the list
    public View getView(int position, View convertView, ViewGroup parent) {

        //get the petition we are displaying
        Troc troc = rentalProperties.get(position);

        //get the inflater and inflate the XML layout for each item
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.troc_layout, null);

        TextView titre = (TextView) view.findViewById(R.id.titre);
        TextView details = (TextView) view.findViewById(R.id.details);
        TextView categorie = (TextView) view.findViewById(R.id.categorie);
        TextView nom = (TextView) view.findViewById(R.id.nom);
        TextView statut = (TextView) view.findViewById(R.id.statut);
        TextView action = (TextView) view.findViewById(R.id.action);
        ImageView image = (ImageView) view.findViewById(R.id.avatar);

        View conteneur = (View) view.findViewById(R.id.infoSection);

        //set address and description
        String toutDetails = troc.getAuteur() + ", " + troc.getEdition();
        details.setText(toutDetails);

        titre.setText(troc.getTitre());
        categorie.setText(troc.getCategorie());
        nom.setText(troc.getNom());
        statut.setText(troc.getStatut());
        action.setText(troc.getAction());

        //get the image associated with this petition
        int imageID = context.getResources().getIdentifier(troc.getImage(), "drawable", context.getPackageName());
        image.setImageResource(imageID);

        System.out.println(action.getText());

        if ("A OFFRIR".equals(action.getText())) {
            conteneur.setBackgroundColor(Color.parseColor("#944c8c"));
            action.setTextColor(Color.parseColor("#944c8c"));
        }
        else if ("A LOUER".equals(action.getText())) {
            conteneur.setBackgroundColor(Color.parseColor("#009874"));
            action.setTextColor(Color.parseColor("#009874"));
        }
        else if ("A ECHANGER".equals(action.getText())) {
            conteneur.setBackgroundColor(Color.parseColor("#F08213"));
            action.setTextColor(Color.parseColor("#F08213"));
        }

        return view;
    }
}
